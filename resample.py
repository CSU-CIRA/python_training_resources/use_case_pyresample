import cartopy.crs as ccrs
import h5py
import matplotlib.pyplot as plt
import numpy as np
from pyresample import geometry, kd_tree

DATA_FILENAME = "VJ102IMG.A2021249.2106.002.2021250095257.nc"
GEO_FILENAME = "VJ103IMG.A2021249.2106.002.2021250085810.nc"

RAD_MIN = 0
RAD_MAX = 520


def main():
    # Read our radiances and geo-navigation arrays
    rads = read_radiances(DATA_FILENAME)
    lons, lats = read_geolocation(GEO_FILENAME)

    # Pyresample uses "AreaDefinitions" to define coordinate data that you need
    # to resample from/to. The "SwathDefinition" is used for our any collection
    # of lats/lons.
    source_def = geometry.SwathDefinition(lons=lons, lats=lats)

    destination_lons_1d = np.linspace(-126.0, -123.0, 500)
    destination_lats_1d = np.linspace(28.0, 31.0, 500)
    lons_2d, lats_2d = np.meshgrid(destination_lons_1d, destination_lats_1d)
    destination_def = geometry.SwathDefinition(lons=lons_2d, lats=lats_2d)

    # We use kd_tree to perform our resampling.  We're using "gaussian"
    # resampling which is a type of distance weighted average where the weights
    # fall-off using a gaussian distribution:
    # https://pyresample.readthedocs.io/en/latest/api/pyresample.html#pyresample.kd_tree.resample_gauss
    # Here we're indicating that all source points within 20km of each
    # destination point will be considered in the average. However, this is
    # limited by the neighbors parameter where a maximum of 8 source points will
    # be considered. I believe only the 8 closest neighbors will be considered.
    # The sigmas parameter controls the falloff of the gaussian curve using the
    # following formula: exp(-dist^2/sigma_k^2). If no source points can be
    # found within the radius of influence for a destination point, then it will
    # be filled in with "fill_value". Setting "fill_value" to "None" makes
    # pyresample fill missing values with NaN.
    rads_on_grid = kd_tree.resample_gauss(source_def,
                                          rads,
                                          destination_def,
                                          radius_of_influence=20000,
                                          neighbours=8,
                                          sigmas=10000,
                                          fill_value=None)

    # Plot the array values as they appear in memory.
    fig, ax = plt.subplots()
    ax.imshow(rads_on_grid,
              vmin=RAD_MIN,
              vmax=RAD_MAX,
              origin="lower",
              interpolation="nearest")
    fig.savefig("raw_grid.png")
    fig.clf()

    # Frequently we need our data to be resampled to an equally spaced grid in a
    # particular projection.  We specify our projection using PROJ projections:
    # https://proj.org/operations/projections/index.html.  We need to specify
    # our projection parameters in a dictionary or as a "proj4" string.
    proj_dict = {
        'proj': 'merc',
        'lat_0': 0,
        'lon_0': 0,
        'a': 6371228.0,
        'units': 'm'
    }

    # The geometry.AreaDefiinition class has a number of helper methods for
    # generating AreaDefinitions:
    # https://pyresample.readthedocs.io/en/latest/api/pyresample.html#pyresample.geometry.AreaDefinition
    # from your projection parameters and any other information you have about
    # your destination grid.  Here we're specifying our AreaDefinition with our
    # projection parameters, the number of samples in each direction we want
    # (500x500), and the extents of the projection specified in degrees (you can
    # also use meters).
    area_def = geometry.AreaDefinition.from_extent("merc_example",
                                                   proj_dict, [500, 500],
                                                   [-117.0, 28.0, -114.0, 31],
                                                   units="degrees")

    # Resample using nearest-neighbor resampling.
    # https://pyresample.readthedocs.io/en/latest/api/pyresample.html#pyresample.kd_tree.resample_nearest
    # This has similar arguments to the gauss resampling.  Here we're using an
    # epsilon of 20m which allows for an error tolerance when it determines the
    # closest point.  This reduces accuracy but can speed up the resampling
    # calculation.
    rads_on_merc = kd_tree.resample_nearest(source_def,
                                            rads,
                                            area_def,
                                            radius_of_influence=20000,
                                            epsilon=20,
                                            fill_value=None)

    # AreaDefinitions can provide a Cartopy CoordinateReferenceSystem so that
    # we can plot our data on a map.
    crs = area_def.to_cartopy_crs()
    fig, ax = plt.subplots(subplot_kw=dict(projection=crs))
    ax.coastlines()
    ax.set_global()
    ax.imshow(rads_on_merc,
              transform=crs,
              extent=crs.bounds,
              origin='upper',
              interpolation="nearest")
    ax.gridlines(xlocs=np.arange(-180, 181, 1),
                 ylocs=np.arange(-90, 90, 1),
                 draw_labels=True,
                 linewidth=1,
                 color='gray',
                 alpha=1,
                 linestyle='--')
    fig.savefig('on_map.png')
    fig.clf()


def read_radiances(filename):
    with h5py.File(filename, "r") as in_file:
        var = in_file["observation_data/I01"]
        scale_factor = var.attrs["radiance_scale_factor"]
        offset = var.attrs["radiance_add_offset"]

        rads = var * scale_factor
        rads += offset

        mask = (rads < RAD_MIN) | (rads > RAD_MAX)
        rads[mask] = np.nan

        return rads


def read_geolocation(filename):
    with h5py.File(filename, "r") as in_file:
        lons = in_file["geolocation_data/longitude"][:]
        lats = in_file["geolocation_data/latitude"][:]

        return lons, lats


if __name__ == "__main__":
    main()
